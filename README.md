# self-class-l

# library 
- gulp 
- bootstrap

# connection to database
```
 - edit ->  /includes/conn.php
 - look for $constring = 'mysql:host=localhost;dbname=test'; 
 - replace test with your own database name
```
# gulp

```
 - edit -> gulpfile.js
 - look for


var paths = {
    styles: {
        src: 'style.scss', 
        dest: 'assets/css/'
    },
    scripts: {
        src: [
            'app.js'
        ],
        dest: 'assets/js/'
    },
    site: {
        url: 'http://localhost/self-class-library/'
    }
}

 - configure your own paths and save
  usage: 
  css
  src: 'style.scss', - > location of your scss
  dest: 'assets/css/' - > destination of where scss file will be converted output -> asset/css/style.scss

  js
   'app.js' -> location of your js
   dest: 'assets/js/' destination of where js file will be converted output-> asset/js/app.js
```


# PHP functions usage
```
include the nesesarry files
include('includes/conn.php');
include('includes/functions.php');

usage

GET usage
$data = get('data');
foreach ($data as $row) {
    echo $row['name']."<br />\n";
}


GET where id 
$data = get_whereid('data' , '1');
foreach ($data as $row) {
    echo $row['id']."<br />\n";
}
get where field
$data = get_where_fieldvalue('data','name','test');
foreach ($data as $row) {
    echo $row['name']."<br />\n";
}

insert function usage
$array = array(
	'name'=>'value'
);
if(insert($array,'data')){
	echo 'inserted';
}else{
	echo 'error on insert';
}

update function usage
$array = array(
	'name'=> 'value2'
);
if(update($array,'3','data')){
	echo 'updated';
}else{
	echo 'error on update';
}

delete function usage
if(delete('3','data')){
	echo 'deleted';
}else{
	echo 'echo on query';
}

custom query usage
$data = custom_query('select * from test');
foreach ($data as $row) {
    echo $row['id']."<br />\n";
}

count function usage
$count = count_rows('data');
echo $count;

excerpt function usage
$text = "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsa delectus exercitationem dolore, inventore at, autem molestiae dolorem, possimus ullam reiciendis suscipit rerum? Error quam rerum esse minus minima enim molestias.";
$text = display_excerpt($text , 10);
echo $text;
```

# update
```
October 24 , 2019 
-refix file architecture
-auto import php files on includes folder
-fix gulpjs watch 
-created partial templates
-added declarable variable section on functions

```
```
Aug 25 2020
- added backend UI
- fix issues bootstrap reference
- added class lib and connection to function/backend reference
```

# Author

<h3>Czar G.</h3> 
<h3>Head Technical | Mydevhouse | Freelance Web Developer</h3> 
<h3><a href="https://mydevhouse.dev">https://mydevhouse.dev</a></h3> <br>
<a href="https://www.facebook.com/appleideath" target="_blank"><img src="https://img.icons8.com/doodle/50/000000/facebook-new.png"></a>
<a href="https://www.reddit.com/r/appleideath" target="_blank"><img src="https://img.icons8.com/doodle/48/000000/reddit--v4.png"></a>
<a href="https://twitter.com/appleideathxx" target="_blank"><img src="https://img.icons8.com/cotton/48/000000/twitter.png"></a>
<a href="https://www.instagram.com/appleideath/" target="_blank"><img src="https://img.icons8.com/officel/64/000000/instagram-new.png"></a>
<a href="https://gitlab.com/czar.gaba" target="_blank"><img src="https://img.icons8.com/color/48/000000/gitlab.png"></a>
<a href="https://www.linkedin.com/in/czar-gaba-b67a24124/" target="_blank"><img src="https://img.icons8.com/doodle/48/000000/linkedin--v2.png"></a>

<p> Love is the Death of Duty</p>